## [1.2.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.2.1...v1.2.2) (2024-09-05)


### Bug Fixes

* **richdocuments:** Update to 8.4.6, ([986f961](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/986f96183ef5e4f3a5d33b318b94ccc4b0d97a3c))

## [1.2.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.2.0...v1.2.1) (2024-09-04)


### Bug Fixes

* Bump apache2 and curl packages. ([a8e09fd](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/a8e09fd90bbbee3cc847b9114b00c4ef279d3858))
* Nextcloud 29.0.6. ([5951b18](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/5951b18b10f025cf8f3a5ead62786f374e292f8c))

# [1.2.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.24...v1.2.0) (2024-08-21)


### Features

* Nextcloud 29.0.5. ([2c73ae3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/2c73ae3a2bb44a67bc2153db7e72c6a764445451))

## [1.1.24](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.23...v1.1.24) (2024-07-23)


### Bug Fixes

* Bump base layer to 1.4.14. ([8b5247d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/8b5247dfd1e86cca3ef6f04a677c1129d7121e05))

## [1.1.23](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.22...v1.1.23) (2024-07-23)


### Bug Fixes

* Bump apache2 to 2.4.61-1~deb12u1. ([290562a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/290562a046416717317af8f8fb761803dc714b5c))
* Bump base layer to 1.4.13. ([fc64879](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/fc64879e3b0f3cd08c95730aaa5c309937ee6ebf))

## [1.1.22](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.21...v1.1.22) (2024-07-03)


### Bug Fixes

* Update to 28.0.7 including latest apps for 28. ([a9c2ec0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/a9c2ec044b02a07eeabc9fa8ddb1824b14523fc2))
* Update to 28.0.7 including latest apps for 28. ([bf09add](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/bf09addffe40ba559fd4ef7a22c19ef66c6889f9))
* Update to 28.0.7 including latest apps for 28. ([6615642](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/66156425bd7cf8833c1ff6f76d86d8c8ddfa9994))

## [1.1.21](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.20...v1.1.21) (2024-05-07)


### Bug Fixes

* **nextcloud:** Bump base-layer to 28.0.5 and latest 28-release apps ([9dc6ea5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/9dc6ea50cf159006785969aabac0e1d4a655f9b5))

## [1.1.20](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.19...v1.1.20) (2024-05-06)


### Bug Fixes

* **apache:** Bump to 2.4.59-1 ([00b243f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/00b243f9daf890db6d6cc9a5c999844999c09965))
* **integration_swp:** Bump to 3.1.16 ([e6e0a31](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/e6e0a318b6241211914bbb92177dd4a248ec2084))

## [1.1.19](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.18...v1.1.19) (2024-04-02)


### Bug Fixes

* **core:** Bump to 28.0.4 ([a84297f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/a84297fe99621f7b18800a09c39703ffdbaef30b))

## [1.1.18](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.17...v1.1.18) (2024-03-26)


### Bug Fixes

* **baselayer:** Bump to include NC 28.0.3 ([020a214](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/020a214b4b9455f50543fc4d864c85cd85433e7b))

## [1.1.17](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.16...v1.1.17) (2024-03-18)


### Bug Fixes

* **baselayer:** Bump to 1.4.7 ([4cc00eb](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/4cc00eb2ac54e9a8f6b3228a8ba5a0b2e1f5e970))

## [1.1.16](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.15...v1.1.16) (2024-03-11)


### Bug Fixes

* **image:** Bump base layer ([99db76d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/99db76d811ec549c1924400efb98359f7d649974))

## [1.1.15](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.14...v1.1.15) (2024-02-27)


### Bug Fixes

* Update base layer to 1.4.5 ([1606b5c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/1606b5ca6e05ae7cfab8aba803651cbe63b1ccd3))

## [1.1.14](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.13...v1.1.14) (2024-02-15)


### Bug Fixes

* **dockerfile:** Create FROM for artifacts layer to help renovate ([455aa07](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/455aa07ae50c7c5ea0a07e9f7a5385a89dfa71e6))

## [1.1.13](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.12...v1.1.13) (2024-02-15)


### Bug Fixes

* **Dockerfile:** Update nextcloud base to v1.4.4 ([26f09df](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/26f09df5357d40195fa1731004b402f9dc40b278))

## [1.1.12](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.11...v1.1.12) (2024-02-15)


### Bug Fixes

* **Dockerfile:** Update debian:bookworm Docker digest to fc27237 ([821bef4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/821bef43d4dc634f104995f6fd2e0b4ee2982069))
* **Dockerfile:** Update gcr.io/distroless/base-debian12:nonroot Docker digest to 1a6f64f ([08d55ea](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/08d55ea2d3011ee58281d8a52085d405d23e1cab))

## [1.1.11](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.10...v1.1.11) (2024-02-04)


### Bug Fixes

* **Dockerfile:** Add --no-install-recommends ([d6d23f6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/d6d23f6bfacdb8ad3a4f3d0f891303b545d06d7a))

## [1.1.10](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.9...v1.1.10) (2024-02-04)


### Bug Fixes

* **Dockerfile:** Remove debugging commands in Dockerfile ([4fe3fc1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/4fe3fc1bcd7d7ce3cb99a360149e7793b19682ff))
* **Dockerfile:** Update base-layer to 1.4.3 ([37bf5e9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/37bf5e9f33fcd457f99011943b2144baf74b075a))

## [1.1.9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.8...v1.1.9) (2024-01-31)


### Bug Fixes

* **Dockerfile:** Update base to 1.4.2 ([377ee60](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/377ee608fdd76a5b83d9258faae8d8f718b07ca8))

## [1.1.8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.7...v1.1.8) (2024-01-30)


### Bug Fixes

* **Dockerfile:** Update nextcloud-base to 1.4.0 ([ba1a964](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/ba1a964eb96d3746ca89248a0582587fb303712b))

## [1.1.7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.6...v1.1.7) (2024-01-02)


### Bug Fixes

* **src:** Fix copy directory structure ([6089b0b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/6089b0b0e39ed0e209653336ed420b120ec02233))

## [1.1.6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.5...v1.1.6) (2024-01-02)


### Bug Fixes

* **src:** Add mimetypes to apache ([948bb54](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/948bb54db3faee2975eecea1f3c35fe962e024e9))

## [1.1.5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.4...v1.1.5) (2023-12-29)


### Bug Fixes

* **src:** Fix indent of CustomLog ([8d675a0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/8d675a0246dd5c9dc3a8fc30cbe44864af10eb7f))

## [1.1.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.3...v1.1.4) (2023-12-28)


### Bug Fixes

* **ci:** Update gitlab-config to v2.3.1 ([5952d9f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/5952d9ff6c1677c7b820e8f485b0588d0b24a355))
* **Dockerfile:** Use Nextcloud base v1.3.4 ([f985dc8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/f985dc8e0e0bcda6450a1bf9ace3a0e18ac05c4a))

## [1.1.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/compare/v1.1.2...v1.1.3) (2023-12-28)


### Bug Fixes

* **Dockerfile:** Update curl to 7.88.1-10+deb12u5 ([bcbbcee](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/bcbbceec375913dea5b2f3f73dc33f4c3462d1e0))
* **Dockerfile:** Update Nextcloud artifacts to v1.3.1-nextcloud-27 ([53baa85](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/53baa850d7bf64281b9b6ffc6a0c8dd79fd668c2))
* **Dockerfile:** Use Nextcloud base v1.3.3 ([0b40680](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2/commit/0b4068025138b7e3a7fa4da1a449d577f5410375))

## [1.1.3](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/compare/v1.1.2...v1.1.3) (2023-12-27)


### Bug Fixes

* **Dockerfile:** Update curl to 7.88.1-10+deb12u5 ([bcbbcee](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/commit/bcbbceec375913dea5b2f3f73dc33f4c3462d1e0))
* **Dockerfile:** Update Nextcloud artifacts to v1.3.1-nextcloud-27 ([53baa85](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/commit/53baa850d7bf64281b9b6ffc6a0c8dd79fd668c2))

## [1.1.2](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/compare/v1.1.1...v1.1.2) (2023-12-23)


### Bug Fixes

* **Dockerfile:** Update Nextcloud artifacts to v1.2.2 ([b528da8](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/commit/b528da8d2fd24a9a5fed5b551a580991af05068e))

## [1.1.1](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/compare/v1.1.0...v1.1.1) (2023-12-22)


### Bug Fixes

* **Dokcerfile:** Update Nextcloud artifacts to v1.2.1 ([eafb923](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/commit/eafb9233e934280b55f0ad8574cb22d59b741ee6))

# [1.1.0](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/compare/v1.0.0...v1.1.0) (2023-12-19)


### Bug Fixes

* **Dockerfile:** Add missing nextcloud artifact layer ([6bd84fd](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/commit/6bd84fd1db52b76d59438bcbf62c176f901b2f2d))


### Features

* **Dockerfile:** Replace httpd image with a multistage build ([1c8d001](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/commit/1c8d001febd9de43a1cc50c90748a28c6e2fd01c))

# 1.0.0 (2023-12-16)


### Bug Fixes

* **Dockerfile:** Remove explicit copy list ([21586df](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/commit/21586df85cde0a14f890af27449ba09f6370cc81))
* **Dockerfile:** Remove PHP image from FROM ([652426b](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/commit/652426bf849531bfd899a658a2a179fe1bc312dc))
* **Dockerfile:** Use explicit COPY --from image tag ([d6e8f7f](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/commit/d6e8f7f7952dd4b19569f82494d395ff8b7062d9))
* **Dockerfile:** Use opendesk-httpd as base image ([74f6546](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/commit/74f6546cd625d4226c259f6755c4f90acb95517d))


### Features

* **opendesk-nextcloud-httpd:** Initial commit ([e6c0658](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/commit/e6c065805275c715460433d59d11418c4159e850))

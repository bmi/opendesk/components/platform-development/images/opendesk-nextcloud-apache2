# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-FileCopyrightText: 2023 Dataport AöR
# SPDX-License-Identifier: EUPL-1.2
FROM debian:bookworm@sha256:fc27237003ad8ffa4608590057eed4e1db8ce6098434b5ecda23f533a5b12e56 AS build

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update \
 && apt-get -y install --no-install-recommends \
    python3=3.11.2-1+b1 \
    apache2=2.4.62-1~deb12u1 \
    curl=7.88.1-10+deb12u7 \
 && apt-get clean \
 && /usr/sbin/adduser --uid 65532 --shell /sbin/nologin --home /nonexistent --no-create-home nonroot;

RUN mkdir -p /output/etc/apache2/conf/ && cp /etc/apache2/magic /output/etc/apache2/conf/magic \
 && rm -rf /etc/apache2/* \
 && cp -R /etc/apache2/ /output/etc/ \
 && mkdir -p /output/usr/share/ && cp -R /usr/share/apache2/ /output/usr/share/ \
 && mkdir -p /output/var/lib/ && cp -R /var/lib/apache2/ /output/var/lib/ \
 && cp /etc/passwd /output/etc/passwd; cp /etc/group /output/etc/group; cp /etc/mime.types /output/etc/mime.types

COPY src/files/etc/apache2/ /output/etc/apache2/
COPY --chown=65532:65532 src/files/var/www/html/ /output/var/www/html/

COPY src/build/ /build/
WORKDIR /build
RUN python3 ldd-copy.py /output/ /usr/bin/ab /usr/bin/ht* /usr/sbin/a2* /usr/sbin/apache* /usr/lib/apache2/modules/* /usr/bin/curl \
 && cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime \
 && python3 /build/dpkg-info-copy.py \
 && mkdir -p /output/var/www/html/; rm -rf /output/var/www/html/*; rm -rf /output/lib64;

FROM registry.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-base-layer:1.5.2@sha256:5966366aea510e472847fcffc6d41f1d84dfb9f5e5b21994bf09bece602ce8b1 AS artifacts

FROM gcr.io/distroless/base-debian12:nonroot@sha256:1a6f64f3fed75597fda0fa9cf8e60c3f6b029dedcef57071a2f42975a1b2bf8a

LABEL org.opencontainers.image.authors="Bundesministerium des Innern und für Heimat" \
      org.opencontainers.image.documentation=https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd/-/blob/main/README.md \
      org.opencontainers.image.source=https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-httpd \
      org.opencontainers.image.vendor="Bundesministerium des Innern und für Heimat" \
      org.opencontainers.image.licenses=EUPL-1.2 \
      org.opencontainers.image.base.name=gcr.io/distroless/base-debian12:nonroot \
      org.opencontainers.image.base.digest=sha256:1a6f64f3fed75597fda0fa9cf8e60c3f6b029dedcef57071a2f42975a1b2bf8a

COPY --from=build output/ /
COPY --from=artifacts var/www /var/www

USER nonroot:nonroot
ENV LANG de_DE.UTF-8
ENV HTTPD_PREFIX /usr/local/apache2
ENV PATH $HTTPD_PREFIX/bin:$PATH
WORKDIR $HTTPD_PREFIX
STOPSIGNAL SIGWINCH
EXPOSE 80
HEALTHCHECK CMD /usr/bin/curl --fail localhost:8081/server-status || exit 1
CMD ["/usr/sbin/apache2", "-DFOREGROUND"]

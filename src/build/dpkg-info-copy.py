# SPDX-FileCopyrightText: 2023 Dataport AöR
# SPDX-License-Identifier: EUPL-1.2
import glob
import os.path
import shutil

DPKG_FILES_DIR = "/var/lib/dpkg"
TARGET_BASE_DIR = "/output/"
DPKG_OUTPUT_DIR = os.path.join(TARGET_BASE_DIR.rstrip("/"), DPKG_FILES_DIR.lstrip("/"))

file_list = glob.glob(DPKG_FILES_DIR + "/info/*.list")
output = []
package_files = []

# check all info files for packages from which files are in the image
for filename in file_list:
    lines = open(filename, "r").readlines()

    found = False

    for l in lines:
        l = l.strip()
        if l == "":
            continue
        l = os.path.join(TARGET_BASE_DIR, l.lstrip("/"))

        if os.path.isfile(l):
            found = True
            break

    if found:
        file_prefix = ''.join(filename.rsplit('.list', 1))
        output.append(file_prefix)

        # add all files from this package
        for l in lines:
            l = l.strip()
            if l == "":
                continue
            if os.path.isfile(l):
                package_files.append(l)

TARGET_INFO_DIR=os.path.join(DPKG_OUTPUT_DIR, "info/")
os.makedirs(TARGET_INFO_DIR, exist_ok=True)


# copy info files of found packages
print("copying", len(output), "of", len(file_list), "files")
for f in output:
    files = glob.glob(f + ".*")
    for f2 in files:
        print("Copying", f2, "to", TARGET_INFO_DIR)
        shutil.copy2(f2, TARGET_INFO_DIR)


print("copying", len(package_files), "additional package files")
for f in package_files:
    dest = os.path.join(TARGET_BASE_DIR, f.lstrip("/"))
    os.makedirs(os.path.dirname(dest), exist_ok=True)
    print("Copying", f, "to", dest)
    shutil.copy2(f, dest)


# parse package names
packages = []
for f in output:
    package = os.path.basename(f)
    if ":" in package:
        package = "".join(package.rsplit(":", 1)[:1])
    packages.append(package)


# dpkg status file

print("Parsing status file")
content = open(os.path.join(DPKG_FILES_DIR, "status"), "r").read()
parts = content.split("\n\n")
status_content = {}

# parse parts from the status file based on existence
for p in parts:
    if p.strip() == "":
        continue

    lines = p.split("\n")
    name = ""

    for l in lines:
        if l.startswith("Package: "):
            name = l.replace("Package: ", "")
            break
    if name != "":
        status_content[name] = p


status_output_list = []
for p in packages:
    if p not in status_content:
        print(p, "missing")
        exit(1)

    status_output_list.append(status_content[p])


# generate new status file and write it to disk
print("Writing dpkg status file to", os.path.join(DPKG_OUTPUT_DIR, "status"))
status_output = "\n\n".join(status_output_list) + "\n"
with open(os.path.join(DPKG_OUTPUT_DIR, "status"), "w") as fp:
    fp.write(status_output)

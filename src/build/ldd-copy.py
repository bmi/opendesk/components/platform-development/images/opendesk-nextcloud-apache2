# SPDX-FileCopyrightText: 2023 Dataport AöR
# SPDX-License-Identifier: EUPL-1.2
import re
import subprocess
import sys
import os
import shutil

if len(sys.argv) < 3:
    print("No path given")
    exit(1)

output_dir = sys.argv[1].rstrip("/")

libs = []

def extract_libraries(bin_file, output_path):
    global libs
    if not os.path.exists(bin_file):
        print("Binary", bin_file, "not found. Exiting.")
        exit(1)

    print("Copying libraries for", bin_file, "to", output_path)

    cmd = ("/usr/bin/ldd", bin_file)
    p = subprocess.run(cmd, capture_output=True, text=True)
    lines = p.stdout.split('\n')
    libs.append(bin_file)

    for line in lines:
        line = line.strip()
        result = re.search(r"(lib.*\.so.*)\s=>\s(\/.*\/\S+).*", line)
        result2 = re.search(r"^(\S*\.so\S*)(?!\s=>\s)\s\(.*\)$", line)
        if result is None and result2 is None:
            continue
        if result is not None and len(result.groups()) == 2:
            libs.append(result.groups()[1])
        if result2 is not None and len(result2.groups()) == 1:
            lib = result2.groups()[0]
            if lib.startswith("/"):
                libs.append(lib)

def copy_files(output_path):
    global libs
    for lib in libs:
        print("Copying", lib)
        lib_output_path = output_path + os.path.dirname(lib)
        lib_output_filename = output_path + lib
        os.makedirs(lib_output_path, exist_ok=True)
        shutil.copy2(lib, lib_output_filename)

bins = sys.argv[2:]

for bin in bins:
    if not os.path.isfile(bin):
        continue
    extract_libraries(bin, output_dir)

# deduplicate
libs = list(dict.fromkeys(libs))

copy_files(output_dir)
